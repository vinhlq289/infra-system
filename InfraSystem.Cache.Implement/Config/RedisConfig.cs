﻿using InfraSystem.Shared.RedisCache.Interfaces;

namespace InfraSystem.Cache.Redis.Implement.Config
{
    public class RedisConfig : IRedisConfig
    {
        public string Server { get; set; }
        public string Password { get; set; }
        public int DbId { get; set; }
        public int LogDbId { get; set; }
    }
}
