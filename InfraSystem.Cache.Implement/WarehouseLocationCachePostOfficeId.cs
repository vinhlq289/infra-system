﻿using InfraSystem.Cache.Interface;
using InfraSystem.Cache.Model;
using InfraSystem.Shared.RedisCache.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Redis.Implement
{
   public class WarehouseLocationCachePostOfficeId : BaseCache<IList<WarehouseLocationPostOffice>, int>, IWarehouseLocationCachePostOfficeId
    {
        private const string KEY = "warehouse-location-postOfficeId";

        public WarehouseLocationCachePostOfficeId(IRedisStorage redisStorage)
            : base(redisStorage, KEY)
        {
        }

        public async Task<bool> StringSetByPostOfficeId(string key, IList<WarehouseLocationPostOffice> models, string languageId = "")
        {
            return await redisStorage.StringSet(key, models);
        }
    }
}
