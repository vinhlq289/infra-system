﻿using InfraSystem.Cache.Interface;
using InfraSystem.Cache.Model;
using InfraSystem.Shared.RedisCache.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Redis.Implement
{
   public class WarehouseLocationCacheUnknow : BaseCache<IList<WarehouseLocationUnkown>, int>, IWarehouseLocationCacheUnknow
    {
        private const string KEY = "warehouse-location-unknow";

        public WarehouseLocationCacheUnknow(IRedisStorage redisStorage)
            : base(redisStorage, KEY)
        {
        }

        public async Task<bool> StringSetByUnknow(string key, IList<WarehouseLocationUnkown> models, string languageId = "")
        {
            return await redisStorage.StringSet(key, models);
        }

        public async Task<IList<WarehouseLocationUnkown>> StringGetByUnknow(string key)
        {
           var data = await redisStorage.StringGet<IList<WarehouseLocationUnkown>>(key);

            return data;
        }
    }
}
