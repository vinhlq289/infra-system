﻿using System;
using System.Collections.Generic;
using System.Text;
using InfraSystem.Common;
using InfraSystem.Wh.Domain;
using Order = InfraSystem.Om.Domain.Order;
using OrderDetail = InfraSystem.Om.Domain.Orderdetail;

namespace InfraSystem.MQ.Worker.AutoMappers
{
    public static class PackageAdapter
    {
        public static PackageDetail ToPackageDetailFromOrderEcm(
            Order request, 
            List<OrderDetail> orderDetails,
            Package package,
            string userId)
        {
            var packageDetail = new PackageDetail
            {
                WarehouseId = package.WarehouseId,
                WarehouseCode = package.WarehouseCode,
                OrderCode = request.Code,
                BuyFee = (int?)request.BuyFee,
                ShippingUnitGlobal = request.ShippingUnitGlobal,
                ShippingFee = request.ShippingFee,
                CurrencyCode = "JPY",
                CurrencyRate = request.ExchangeRate,
                Total = request.Total,
                Paid = request.Paid,
                Surcharge = request.Surcharge,
                SurchargeNote = request.SurchargeNote,
                OrderDate = request.OrderDate,
                OrderType = request.OrderType,
                CustomerId = request.AccountId,
                CustomerName = request.CustomerName,
                CustomerPhone = request.CustomerPhone,
                CustomerProvince = request.CustomerProvince,
                CustomerDistrict = request.CustomerDistrict,
                CustomerWard = request.CustomerWard,
                CustomerAddress = request.CustomerAddress,
                Status = EnumDefine.PackageDetailStatus.Untapped.GetHashCode(),
                ProcessedDate = DateTime.UtcNow,
                Length = request.Length,
                Width = request.Width,
                Height = request.Hight,
                Weight = request.Weight / 1000m,
                PackageId = package.Id,
                RefOrderId = request.Id,
                FromRoute = request.FromRoute,
                Supporter = request.Supporter,
                Note = request.Note
            };

            var products = new List<Product>();
            foreach (var item in orderDetails)
            {
                var product = new Product
                {
                    SourceId = item.Id,
                    Code = item.Code,
                    Name = item.ProductName,
                    Images = item.Images,
                    Price = item.Price,
                    Tax = item.Tax,
                    QtyPerUnit = item.Amount,
                    ProductUnit = item.AmountUnit.ToString(),
                    CustomerNote = item.Note,
                    ManufacturerPartNumber = item.BarCode,
                    NameCustom = item.ProductType,
                    OriginCustom = item.ProductOrigin,
                    Url = item.ProductLink,
                    CreatedBy = userId,
                    CreatedDate = DateTime.UtcNow
                };

                if (package.Id > 0)
                {
                    product.PackageId = package.Id;
                }
                else
                {
                    product.Package = package;
                }

                products.Add(product);
            }

            packageDetail.Product = products;

            var packageDetailMapping = new PackageDetailMapping();
            if (!package.Id.Equals(0))
            {
                packageDetailMapping.PackageId = package.Id;
            }
            else
            {
                packageDetailMapping.Package = package;
            }
            packageDetail.PackageDetailMapping.Add(packageDetailMapping);

            return packageDetail;
        }
    }
}
