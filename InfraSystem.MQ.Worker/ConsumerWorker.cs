﻿using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace InfraSystem.MQ.Worker
{
    public class ConsumerWorker : IHostedService
    {
        private readonly ILogger<ConsumerWorker> _logger;
        private readonly IBusControl _busControl;

        public ConsumerWorker(ILogger<ConsumerWorker> logger, IBusControl busControl)
        {
            _logger = logger;
            _busControl = busControl;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting the bus...");
            return _busControl.StartAsync(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping the bus...");
            return _busControl.StopAsync(cancellationToken);
        }
    }
}
