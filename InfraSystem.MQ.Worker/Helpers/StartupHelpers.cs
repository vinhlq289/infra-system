﻿using System;
using GreenPipes;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using InfraSystem.MQ.Worker.Configs;
using InfraSystem.MQ.Worker.Consumer;
using System.Collections.Generic;
using System.Linq;
using Core.Repository.Interface;
using Microsoft.Extensions.Configuration.Json;
using InfraSystem.Shared.Common;
using InfraSystem.Shared.RedisCache.Implements;
using InfraSystem.Cache.Interface;
using InfraSystem.Cache.Redis.Implement;
using InfraSystem.Cache.Redis.Implement.Config;
using InfraSystem.Shared.RedisCache.Interfaces;
using InfraSystem.Shared.Extensions;
using InfraSystem.Driver.WhCode;
using InfraSystem.Wh.Domain;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Core.Repository.Abstract;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;
using InfraSystem.Om.IService;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;
using InfraSystem.Wh.Repository;
using InfraSystem.Wh.Service;
using InfraSystem.Om.Repository;
using InfraSystem.Om.Service;
using OrderService = InfraSystem.Om.Service.OrderService;

namespace InfraSystem.MQ.Worker.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<WarehouseDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString("Warehouse"));
                opt.EnableSensitiveDataLogging();
            }, optionsLifetime: ServiceLifetime.Scoped);
            
            services.AddDbContext<IchibaCustomerContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString("Warehouse"));
                opt.EnableSensitiveDataLogging();
            }, optionsLifetime: ServiceLifetime.Scoped);
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            return services;
        }

        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            var mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {

            });
            services.AddSingleton(mapperConfig.CreateMapper().RegisterMap());
            return services;
        }

        public static IServiceCollection AddQueueServices(
            this IServiceCollection services,
          HostBuilderContext context)
        {
            services.Configure<QueueSettings>(context.Configuration.GetSection("AppSettings:QueueSettings"));
            var serviceProvider = services.BuildServiceProvider();
            var rabbitMqConfig = serviceProvider.GetRequiredService<IOptions<QueueSettings>>().Value;

            services.AddMassTransit(c =>
            {
                c.AddConsumer<AddPackageConsumer>();
                c.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host($"rabbitmq://{rabbitMqConfig.HostName}{rabbitMqConfig.VirtualHost}", h =>
                    {
                       h.Username(rabbitMqConfig.UserName);
                       h.Password(rabbitMqConfig.Password);
                    });

                    cfg.ReceiveEndpoint($"{rabbitMqConfig.WarehousePackageNameQueue}", endpointConfigurator =>
                    {
                        endpointConfigurator.PrefetchCount = 1;
                        endpointConfigurator.UseRateLimit(1000, TimeSpan.FromSeconds(5));
                        endpointConfigurator.UseRetry(r => r.Interval(3, TimeSpan.FromSeconds(3)));
                        endpointConfigurator.Consumer<AddPackageConsumer>(provider);

                    });

                }));
            });

            return services;
        }

        public static IServiceCollection AddMassTransit(
            this IServiceCollection services, 
            IConfiguration configuration)
        {
            services.Configure<QueueSettings>(configuration.GetSection("AppSettings:QueueSettings"));
            var serviceProvider = services.BuildServiceProvider();
            var rabbitMqConfig = serviceProvider.GetRequiredService<IOptions<QueueSettings>>().Value;
            services.AddSingleton<IHostedService, ConsumerWorker>();

            services.AddTransient<AddPackageConsumer>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<AddPackageConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(rabbitMqConfig.HostName, rabbitMqConfig.VirtualHost, h =>
                    {
                        h.Username(rabbitMqConfig.UserName);
                        h.Password(rabbitMqConfig.Password);
                    });

                    cfg.ReceiveEndpoint(rabbitMqConfig.WarehousePackageNameQueue, e =>
                    {
                        e.PrefetchCount = 1;
                        e.UseRetry(r => r.Interval(3, TimeSpan.FromSeconds(3)));
                        e.UseRateLimit(1000, TimeSpan.FromSeconds(5));
                        e.ConfigureConsumer<AddPackageConsumer>(context);
                    });
                });
            });

            services.AddMassTransitHostedService();

            return services;
        }

        public static IServiceCollection AddConfigurationServices(
            this IServiceCollection service,
            HostBuilderContext context)
        {
            var queueSettings = new QueueSettings();
            context.Configuration.GetSection("QueueSettings").Bind(queueSettings);
            service.AddSingleton(queueSettings);

            return service;
        }

        public static IServiceCollection AddPrivateApiDriver(
            this IServiceCollection services,
            HostBuilderContext context)
        {

            #region  ShipmentCode Private API
            //Config
            var hostingConfig = context.Configuration.GetSection(Driver.WhCode.Common.HostingConfig.Hosting).Get<Driver.WhCode.Common.HostingConfig>();
            var masterPrivateApiConfigSection = context.Configuration.GetSection(WhCodeApiConstant.ConfigurationKey);
            services.Configure<WhCodeApiEndPoints>(masterPrivateApiConfigSection);
            services.AddSingleton(masterPrivateApiConfigSection.Get<WhCodeApiEndPoints>() ?? new WhCodeApiEndPoints());

            // HttpClient
            services.AddSingleton<IWhCodeHttpClientFactory, WhCodeHttpClientFactory>(sp =>
            {
                const int retryCount = 3;
                const int exceptionsAllowedBeforeBreaking = 5;
                var baseUrl = hostingConfig.ApisConfig.AutoNumber;
                var logger = sp.GetRequiredService<ILogger<WhCodeHttpClient>>();
                return new WhCodeHttpClientFactory(
                    logger,
                    exceptionsAllowedBeforeBreaking,
                    retryCount,
                    baseUrl: baseUrl);
            });
            services.AddSingleton<IWhCodeHttpClient, WhCodeHttpClient>
                (sp => sp.GetService<IWhCodeHttpClientFactory>().CreateResilientHttpClient());

            #endregion

            return services;
        }

        public static IServiceCollection AddRedisCache(
            this IServiceCollection services, 
            IConfiguration configuration)
        {
            var configurationRoot = (ConfigurationRoot)configuration;
            var configurationProviders = configurationRoot.Providers
                    .Where(p => p.GetType() == typeof(JsonConfigurationProvider)
                                && ((JsonConfigurationProvider)p).Source.Path.StartsWith("appsettings"));

            foreach (IConfigurationProvider item in configurationProviders)
            {
                ConfigSetting.Init<RedisConnectionConfig>(item);
            }

            #region Redis config

            services.AddSingleton(configuration.GetSection("Redis").Get<InfraSystemRedisConfig>());
            services.AddSingleton<RedisConnection<InfraSystemRedisConfig>>();
            services.AddTransient<IRedisStorage<InfraSystemRedisConfig>, RedisStorage<InfraSystemRedisConfig>>();

            services.AddTransient<IRedisStorage, RedisStorage<InfraSystemRedisConfig>>();

            #endregion Redis config

            services.AddTransient<IProductCache, ProductCache>();
            services.AddTransient<IWarehouseLocationCache, WarehouseLocationCache>();
            services.AddTransient<IWarehouseLocationCacheUnknow, WarehouseLocationCacheUnknow>();
            services.AddTransient<IWarehouseLocationCachePostOfficeId, WarehouseLocationCachePostOfficeId>();
            services.AddTransient<IWarehouseLocationCacheCalculator, WarehouseLocationCacheCalculator>();

            return services;
        }

        public static IServiceCollection RegisterServices(
            this IServiceCollection services, 
            IConfiguration configuration)
        {
            #region Warehouse

            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<IPackageDetailRepository, PackageDetailRepository>();
            services.AddTransient<IPackageDetailService, PackageDetailService>();
            services.AddTransient<IPackageRepository, PackageRepository>();
            services.AddTransient<IPackageService, PackageService>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IServiceChargePackageMappingRepository, ServiceChargePackageMappingRepository>();
            services.AddTransient<IServiceChargePackageMappingService, ServiceChargePackageMappingService>();
            services.AddTransient<IServiceChargeRepository, ServiceChargeRepository>();
            services.AddTransient<IServiceChargeService, ServiceChargeService>();
            services.AddTransient<IWarehouseRepository, WarehouseRepository>();
            services.AddTransient<IWarehouseService, WarehouseService>();

            #endregion

            #region OM

            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IOrderDetailRepository, OrderDetailRepository>();
            services.AddTransient<IOrderDetailService, OrderDetailService>();
            services.AddTransient<IOrderServiceRepository, OrderServiceRepository>();
            services.AddTransient<IOrderServiceService, OrderServiceService>();
            services.AddTransient<IOrderServiceMappingRepository, OrderServiceMappingRepository>();
            services.AddTransient<IOrderServiceMappingService, OrderServiceMappingService>();

            #endregion

            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork<WarehouseDbContext>>();

            return services;
        }

    }
}
