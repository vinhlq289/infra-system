﻿using InfraSystem.MQ.Worker.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.Threading.Tasks;

namespace InfraSystem.MQ.Worker
{
    public class Program
    {
        private static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug().MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.File(
                    path: "logs" + System.IO.Path.DirectorySeparatorChar + "log-{date}.txt",
                    outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff s}][{Level:u3}] {NewLine}{Message:lj}{NewLine}{Exception}{NewLine}{NewLine}",
                    fileSizeLimitBytes: 10*1024*1024*8
                   // rollOnFileSizeLimit: true,
                   // encoding: System.Text.Encoding.UTF8
                   )
                .WriteTo.Console(outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff z}][{Level:u3}] {NewLine}{Message:lj}{NewLine}{Exception}{NewLine}")
                .CreateLogger();
            try
            {
                Log.Information("Starting web host");

                await Host.CreateDefaultBuilder(args)
                  .ConfigureAppConfiguration((hostContext, config) =>
                  {
                      config
                          .SetBasePath(Environment.CurrentDirectory)
                          .AddJsonFile("appsettings.json", true, true)
                          .AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", true);
                      config.AddEnvironmentVariables();

                      if (args != null) config.AddCommandLine(args);
                  })
                  .UseSystemd()
                  .UseSerilog()
                  .ConfigureServices((hostContext, services) =>
                  {
                      services
                          .AddDbContext(hostContext.Configuration)
                          .AddConfigurationServices(hostContext)
                          .AddMassTransit(hostContext.Configuration)
                          .RegisterServices(hostContext.Configuration)
                          //.AddRedisCache(hostContext.Configuration)
                          .AddRepositories()
                          .AddUnitOfWork()
                          //.AddMapper()
                          //.AddQueueServices(hostContext)
                          //.AddPrivateApiDriver(hostContext)
                          //.AddEzProductApi(hostContext.Configuration)
                          //.AddTranslateApi(hostContext.Configuration)
                          //.AddMqApiService(hostContext.Configuration)
                          .AddHostedService<ConsumerWorker>();
                  })
                  .Build()
                  .RunAsync();

            }
            catch (System.Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
