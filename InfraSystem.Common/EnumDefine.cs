﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace InfraSystem.Common
{
    public static class EnumDefine
    {
        public enum PackageStatus
        {
            [Display(Name = "Chưa khai thác")]
            Untapped = 1,
            [Display(Name = "Đã đóng container")]
            Boxed = 2,
            [Display(Name = "Đã đóng chuyến bay")]
            Flying = 4,
            [Display(Name = "Đã bay")]
            Flew = 8,
            [Display(Name = "Chưa khai thác")]
            UntappedExploited = 16,
            [Display(Name = "Đã xử lý")]
            ProcessedExploited = 32,
            [Display(Name = "Đã báo giá")]
            QuotePriceExploited = 64,
            [Display(Name = "Tồn kho")]
            Inventory = 128,
            [Display(Name = "Khiếu nại chờ xử lý")]
            WaitingOM = 256,
            [Display(Name = "Khiếu nại đã xử lý")]
            ProcessedOM = 512
        }

        public enum PackageDetailStatus
        {
            [Display(Name = "Chưa khai thác")]
            Untapped = 1,
            [Display(Name = "Chờ thanh toán")]
            WaitForPay = 2,
            [Display(Name = "Đã thanh toán")]
            Paid = 4,
            [Display(Name = "Đã báo giá")]
            OrderPriceQuotes = 16,
            [Display(Name = "Đang giao hàng")]
            Completed = 8,
            [Display(Name = "Đã giao hàng")]
            Shipped = 512,
            [Display(Name = "Lưu kho")]
            Storage = 32,
            [Display(Name = "Khiếu nại chờ xử lý")]
            WaitingOM = 64,
            [Display(Name = "Khiếu nại đã xử lý")]
            ProcessedOM = 128,
            [Display(Name = "Chờ xử lý")]
            WaitingProcess = 256
        }
        public enum StatusServiceChargeMapping
        {
            [Display(Name = "Chưa hoàn thành")]
            Unfinished = 1,
            [Display(Name = "Hoàn thành")]
            Finished = 2
        }
    }
}
