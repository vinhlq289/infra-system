::  MQ API

dotnet publish "./InfraSystem.MQ.Api/InfraSystem.MQ.Api.csproj"   -c Release --output "./InfraSystem.MQ.Api/bin/Release/netcoreapp3.1/publish"  -f "netcoreapp3.1"  -r linux-x64 --self-contained false  -p:EnvironmentName=Production
:: MQ Private

dotnet publish "./InfraSystem.Public.Api/InfraSystem.Private.Api.csproj"   -c Release --output "./InfraSystem.Public.Api/bin/Release/netcoreapp3.1/publish"  -f "netcoreapp3.1"  -r linux-x64 --self-contained false  -p:EnvironmentName=Production
:: Worker

dotnet publish "./InfraSystem.MQ.Worker/InfraSystem.MQ.Worker.csproj"   -c Release --output "./InfraSystem.MQ.Worker/bin/Release/netcoreapp3.1/publish"  -f "netcoreapp3.1"  -r linux-x64 --self-contained false  -p:EnvironmentName=Production


