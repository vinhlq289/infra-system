﻿namespace InfraSystem.Application.WarehouseLocation
{
   public class WarehouseLocationByPostOfficeResponse
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal? CapacityMaxWeight { get; set; }
        public decimal? CapacityMinWeight { get; set; }
        public decimal? CapacityLocation { get; set; }
        public decimal? TotalWeightUsed { get; set; }
        public decimal? TotalWeightRemainig { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public decimal? Length { get; set; }
        public string PostOfficeId { get; set; }
        public int? X { get; set; }
        public int? Y { get; set; }
        public string AreaId { get; set; }
        public string CargoType { get; set; }
        public string CargoSpservice { get; set; }
        public string CargoShippingMethod { get; set; }
        public string CustomerId { get; set; }
        public string ConsigneeCountry { get; set; }
        public string ConsigneeProvince { get; set; }
        public bool? Unknown { get; set; }
        public bool? Status { get; set; }
    }
}
