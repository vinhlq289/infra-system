﻿namespace InfraSystem.Application.WarehouseLocation.Queries.RequestDto
{
   public class QueryLocationForBagsRequest
    {
        public string DispatchNumber { get; set; }
        public decimal? WeightDispatch { get; set; }
        public decimal? LengthDispatch { get; set; }
        public decimal? HeightDispatch { get; set; }
        public decimal? WidthDispatch { get; set; }
        public string LocationName { get; set; }
    }
}
