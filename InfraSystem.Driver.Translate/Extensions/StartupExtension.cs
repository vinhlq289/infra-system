﻿using InfraSystem.Driver.Translate.Common; 
using InfraSystem.Driver.Translate.Services;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace InfraSystem.Driver.Translate.Extensions
{
    public static class StartupExtension
    {
        // USING REFIT
        //public static IServiceCollection AddMasterPrivateServiceRefitApi(this IServiceCollection services, string baseUrl)
        //{

        //    services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        //    services.AddTransient<WebWorkContextDelegatingHandler>();

        //    services.AddRefitClient<InfraSystem.Driver.Translate.Services.IEventTypeService>()
        //       .ConfigureHttpClient(c =>
        //       {
        //           c.BaseAddress = new Uri(baseUrl);
        //       })
        //       .AddHttpMessageHandler<WebWorkContextDelegatingHandler>();

        //    services.AddRefitClient<InfraSystem.Driver.Translate.Services.ICargoSpMasterService>()
        //       .ConfigureHttpClient(c =>
        //       {
        //           c.BaseAddress = new Uri(baseUrl);
        //       })
        //       .AddHttpMessageHandler<WebWorkContextDelegatingHandler>();

        //    services.AddRefitClient<InfraSystem.Driver.Translate.Services.ISpPartnerMasterService>()
        //       .ConfigureHttpClient(c =>
        //       {
        //           c.BaseAddress = new Uri(baseUrl);
        //       })
        //       .AddHttpMessageHandler<WebWorkContextDelegatingHandler>();

        //    return services;
        //}

        //public static IServiceCollection AddMasterPrivateServiceRefitApi(this IServiceCollection services, string baseUrl, IConfiguration configuration)
        //{
        //    // Register Config Setting

        //    // services.Configure<MasterPrivateApiEndPoints>(configuration.GetSection("InFraSystem:PrivateApiConfig"));
        //    // Register AuthrizeClient
        //    // register HttpClient 

        //    // Register Master Services

        //    // Return back serviceCollection
        //    return services;
        //}

        public static IServiceCollection AddTranslateApi(this IServiceCollection services, IConfiguration configuration)
        {
            //Config 
            var configSection = configuration.GetSection("InfraSystem.Driver.Translate.AppSettings");
            services.Configure<AppSettings>(configSection);
            var appSetting =  configSection.Get<AppSettings>();
            services.AddSingleton<AppSettings>(appSetting );

            // HttpClient
            services.AddSingleton<ITranslateDriverResilientHttpClientFactory, TranslateDriverResilientHttpClientFactory>(sp =>
            {
                const int retryCount = 3;
                const int exceptionsAllowedBeforeBreaking = 2;
                string baseUrl = appSetting.BaseUrl;
                var logger  = sp.GetRequiredService<ILogger<TranslateDriverResilientHttpClient>>();
                return new TranslateDriverResilientHttpClientFactory(logger, exceptionsAllowedBeforeBreaking, retryCount, baseUrl: baseUrl);
            });
            services.AddSingleton<ITranslateDriverHttpClient, TranslateDriverResilientHttpClient>
                (sp => sp.GetService<ITranslateDriverResilientHttpClientFactory>().CreateResilientHttpClient());

            // Master Service 
            services.AddSingleton<IAuthorizeService, AuthorizeService>();
            services.AddSingleton<ITranslateService, TranslateService>();
           
            return services;

        }
         
        //private static IServiceCollection AddMasterService<T>(this IServiceCollection services, string baseUrl) where T : class
        //{ 
        //    // services.AddRestEaseClient<IMeasureWeightMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
        //    services.AddRestEaseClient<T>(baseUrl)
        //        .AddHttpMessageHandler<MasterPrivateWebWorkContextDelegatingHandler>()
        //        .AddTransientHttpErrorPolicy(builder =>
        //            builder.WaitAndRetryAsync(new[]
        //            {
        //                TimeSpan.FromMilliseconds(100),
        //                TimeSpan.FromMilliseconds(500),
        //                TimeSpan.FromSeconds(2)
        //            }));
        //    return services;
        //}
    }
}
