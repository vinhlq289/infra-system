﻿namespace InfraSystem.Driver.Translate.Common
{
    public static class StringExtensions
    {

        public static string NullEmpty(this string input)
        {
            return string.IsNullOrWhiteSpace(input) ? null : input.Trim();
        }

    }
}
