﻿using InfraSystem.Driver.MqApi.Request;

using RestEase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Driver.MqApi.IServices
{
    /// <summary>
    /// Kết nối tới API Event Log Mq 
    /// </summary>
    [Header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36")]
    [Header("Accept", "application/json,text/*")]
    public interface IProductMqApi
    {
        /// <summary>
        /// Tringger Event log
        /// </summary>
        /// <param name="eventArgs"></param>
        /// <returns></returns>
        [Post("/api/product/master_product_sync_product_code")]
        Task SyncProductCodeAsync([Body]List<CheckNewProductCodeRequestModel> request);
    }
}
