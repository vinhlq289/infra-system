﻿using System.Collections.Generic;

namespace InfraSystem.Driver.MqApi.Common
{
    public class DataResult
    {
        public bool success { get; set; }

        public string code { get; set; }

        public int httpStatusCode { get; set; }

        public string title { get; set; }

        public string message { get; set; }

        public object data { get; set; }

        public Dictionary<string, IEnumerable<string>> errors { get; set; }

        public DataResult()
        {
            success = true;
            httpStatusCode = 200; 
        }
    }
    public class DataResult<T> : DataResult
    {
        public new T data { get; set; }
    }
}
