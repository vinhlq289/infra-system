﻿using System;

namespace InfraSystem.Driver.MqApi.Request
{
    public class PartnerEventModel
    {
        public int Code { get; set; }
        public string Data { get; set; }
        public DateTime Time { get; set; }

    }
}
