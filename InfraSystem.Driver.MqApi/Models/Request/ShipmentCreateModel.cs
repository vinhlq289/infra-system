﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InfraSystem.Driver.MqApi.Request
{
    public class ShipmentCreateModel
    {
        /// <summary>
        /// Mã tracking của KH
        /// </summary>
        public string ReferenceNumber { get; set; } // varchar(50)
        /// <summary>
        /// Mã Shipment EFEX (bỏ trống khi tạo  mới)
        /// </summary>
        public string ShipmentNumber { get; set; } // varchar(50)

        /// <summary>
        /// True :  hàng đi ngay(Không lưu kho) | False : hàng lưu kho
        /// </summary>
        public bool? IsDirectShipment { get; set; }

        /// <summary>
        /// Thông tin người gửi
        /// </summary>
        public AccountObject Customer { get; set; }


        /// <summary>
        /// Thông tin người gửi
        /// </summary>
        public AccountObject Shipper { get; set; }
        /// <summary>
        /// Thông tin người nhận hàng
        /// </summary>
        public AccountObject Consignee { get; set; }
        /// <summary>
        /// Thông tin lấy hàng
        /// </summary>
        public AccountObject Pickup { get; set; }

        /// <summary>
        /// Hình thức vận chuyển hàng hóa(Enum: AIR; OCEAN, INLAND)(Hàng không , Đường biển , Đường bộ)
        /// </summary>
        public string CargoShippingMethod { get; set; }

        /// <summary>
        /// Danh mục Loại hàng hóa (Commodity Group)
        /// </summary>
        public string CargoType { get; set; }

        /// <summary>
        /// Tổng số kiện hàng
        /// </summary>
        public int Pieces { get; set; } // int

        /// <summary>
        /// Đơn vị đo khối lượng
        /// </summary>
        public string MeasureWeightCode { get; set; } // nvarchar(50)
        /// <summary>
        /// Đơn vị đo thể tích
        /// </summary>
        public string MeasureDimensionCode { get; set; } // varchar(50)

        /// <summary>
        /// Tiền tệ
        /// </summary>
        public string CurrencyCode { get; set; } // nvarchar(50)
        /// <summary>
        /// Bưu Cục nhận hàng
        /// </summary>
        public string PostOfficeToSendCode { get; set; } // nvarchar(50)


        /// <summary>
        /// Loại dịch vụ vận chuyển (Có danh mục)
        /// </summary>
        public string CargoSPServiceCode { get; set; } // nvarchar(50)
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Note { get; set; } // nvarchar(4000)

        /// <summary>
        /// Tổng trọng lượng
        /// </summary>
        public decimal? TotalGrossWeight { get; set; } // decimal(18, 4)
        /// <summary>
        /// Chiều dài
        /// </summary>
        public decimal? Length { get; set; } // decimal(18, 4)
        /// <summary>
        /// Chiều rộng 
        /// </summary>
        public decimal? Width { get; set; } // decimal(18, 4)
        /// <summary>
        /// Chiều cao
        /// </summary>
        public decimal? Height { get; set; } // decimal(18, 4)

        /// <summary>
        /// Tổng giá trị tiền hàng
        /// </summary>
        public decimal TotalAmount { get; set; } // decimal(18, 4)
        /// <summary>
        /// Tổng giá trị khai báo hải quan
        /// </summary>
        public decimal TotalDeclaredCustomsValue { get; set; } // decimal(18, 4)

        /// <summary>
        /// Tổng tiền thu hộ
        /// </summary>
        public decimal? CODAmount { get; set; }

        /// <summary>
        /// Mã đối tác sẽ gửi hàng
        /// </summary>
        public string TruckerCode { get; set; }


        /// <summary>
        /// Chi tiết gói hàng
        /// </summary>
        public List<ShipmentItemModel> ShipmentItems { get; set; }

        /// <summary>
        /// Chi tiết kiện hàng
        /// </summary>
        public IList<ShipmentPieceModel> ShipmentPieces { get; set; }

        /// <summary>
        /// Danh sách dịch vụ kèm theo
        /// </summary>
        public List<CodeModel> ShipmentCargoAddServices { get; set; }

        /// <summary>
        /// Url Hình ảnh (nếu nhiều ảnh thì phân cách nhau bởi dấu ";")
        /// </summary>
        public string Images { get; set; }

        public ShipmentCreateModel()
        {
            ShipmentItems = new List<ShipmentItemModel>();
            ShipmentCargoAddServices = new List<CodeModel>();
            ShipmentPieces = new List<ShipmentPieceModel>();
        }
    }

    /// <summary>
    /// Thông tin  Đối tượng
    /// </summary>
    public class AccountObject
    {
        /// <summary>
        /// ID  đối tượng
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// mã  đối tượng
        /// </summary>
        public string Code { get; set; } // nvarchar(50)
        /// <summary>
        /// Tên đối tượng
        /// </summary>
        public string Name { get; set; } // nvarchar(255)

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; } // nvarchar(255)
        /// <summary>
        /// Mã quốc gia
        /// </summary>
        public string CountryCode { get; set; } // varchar(50)
        /// <summary>
        /// Tên quốc gia
        /// </summary>
        public string Country { get; set; } // varchar(50)
        /// <summary>
        /// Mã hoặc tên tỉnh/ thành phố
        /// </summary>
        public string StateProvince { get; set; } // varchar(50)
        /// <summary>
        /// Mã quyện/huyện
        /// </summary>
        public string District { get; set; } // varchar(50)
        /// <summary>
        /// Mã xã/ phường
        /// </summary>
        public string Ward { get; set; } // varchar(50)
        /// <summary>
        /// Địa chỉ 1
        /// </summary>
        public string Address1 { get; set; } // nvarchar(1000)
        /// <summary>
        /// Địa chỉ 2
        /// </summary>
        public string Address2 { get; set; } // nvarchar(1000)

        /// <summary>
        /// mã PostCode
        /// </summary>
        public string ZipPostalCode { get; set; } // nvarchar(50)
        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string PhoneNumber { get; set; } // nvarchar(50)
    }

    /// <summary>
    /// Thông tin shipment
    /// </summary>
    public class ShipmentItemModel
    {
        [JsonConstructor]
        public ShipmentItemModel(  string commodityCode, string commodityText, string commodityGroupCode, string commodityGroupName, string productCode, string productName, string description, string countryOfOriginCode, string unitPrice, decimal? quantity, decimal? price, decimal? amount, decimal? declaredCustomsPrice, decimal? declaredCustomsAmount, string note, string images)
        { 
            CommodityCode = commodityCode;
            CommodityText = commodityText;
            CommodityGroupCode = commodityGroupCode;
            CommodityGroupName = commodityGroupName;
            ProductCode = productCode;
            ProductName = productName;
            Description = description;
            CountryOfOriginCode = countryOfOriginCode;
            UnitPrice = unitPrice;
            Quantity = quantity;
            Price = price;
            Amount = amount;
            DeclaredCustomsPrice = declaredCustomsPrice;
            DeclaredCustomsAmount = declaredCustomsAmount;
            Note = note;
            Images = images;
        }

        /// <summary>
        /// Mã Nhóm hàng hóa (Danh mục Commodity) 
        /// </summary>
        public string CommodityCode { get; set; } // nvarchar(50)
        /// <summary>
        /// Tên nhóm hàng hóa (Danh mục Commodity) 
        /// </summary>
        public string CommodityText { get; set; } // nvarchar(50)
        /// <summary>
        /// Mã loại hàng hóa (Danh mục Commodity Group) 
        /// </summary>
        public string CommodityGroupCode { get; set; } // nvarchar(50)

        /// <summary>
        /// Tên loại hàng hóa (Danh mục Commodity Group)
        /// </summary>
        public string CommodityGroupName { get; set; } // nvarchar(50)

        /// <summary>
        /// Mã vạch hàng hóa
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// Tên hàng hóa
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// Mô tả chi tiết thành phần hàng hóa 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Nơi xuất xứ (Mã quốc  gia)
        /// </summary>
        public string CountryOfOriginCode { get; set; } // nvarchar(50)
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public string UnitPrice { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public decimal? Quantity { get; set; } // decimal(18, 4)
        /// <summary>
        /// Đơn giá sản phẩm
        /// </summary>
        public decimal? Price { get; set; } // decimal(18, 4)
        /// <summary>
        /// Thành tiền
        /// </summary>
        public decimal? Amount { get; set; } // decimal(18, 4)
        /// <summary>
        /// Đơn giá khai báo hải quan
        /// </summary>
        public decimal? DeclaredCustomsPrice { get; set; } // decimal(18, 4)
        /// <summary>
        /// Tổng giá trị khai báo hải quan
        /// </summary>
        public decimal? DeclaredCustomsAmount { get; set; } // decimal(18, 4)
        
        /// <summary>
        /// Ghi  chú 
        /// </summary>
        public string? Note { get; set; } // nvarchar(500)


        /// <summary>
        /// Hình ảnh
        /// </summary>
        public string Images { get; set; }

       
        /// <summary>
        /// Tính lại tông tiền
        /// </summary>

        public void CalculateAmount()
        {
            if (Amount <= 0 && Price > 0 && Quantity > 0)
            {
                Amount = Price * Quantity;
            }
            if (DeclaredCustomsPrice <= 0 && Price > 0)
            {
                DeclaredCustomsPrice = Price;
            }
            if ((DeclaredCustomsAmount == null || DeclaredCustomsAmount <= 0) && Amount > 0)
            {
                DeclaredCustomsAmount = Amount;
            }
        }

    }

    /// <summary>
    /// Thông tin kiện hàng
    /// </summary>
    public class ShipmentPieceModel
    {
        /// <summary>
        /// Mã tracking ( mặc định = mã tham chiếu của shipment)
        /// </summary>
        public string ReferenceNumber { get; set; }


        /// <summary>
        /// mã kiện:  ShipmentNumber-Seq/Total
        /// </summary>
        public string ShipmentPieceNumber { get; set; }

        /// <summary>
        /// Số thứ tự kiện
        /// </summary>
        public int PieceSequence { get; set; }

        /// <summary>
        /// Chiều dài
        /// </summary>
        public decimal? Length { get; set; }

        /// <summary>
        /// Rộng
        /// </summary>
        public decimal? Width { get; set; }
        /// <summary>
        /// Chiều cao 
        /// </summary>
        public decimal? Height { get; set; }

        /// <summary>
        /// Khối lượng kiện
        /// </summary>
        public decimal? GrossWeight { get; set; }

    }

    /// <summary>
    /// Mã Code
    /// </summary>
    public class CodeModel
    {
        public string Code { get; set; }
    }

}
