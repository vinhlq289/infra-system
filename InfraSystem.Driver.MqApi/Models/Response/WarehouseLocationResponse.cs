﻿namespace InfraSystem.Driver.MqApi.Response
{
    public class WarehouseLocationResponse
    {
        public string ReferenceNumber { get; set; }
        public string LocationName { get; set; }
    }
}
