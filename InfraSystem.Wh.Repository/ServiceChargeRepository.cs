﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class ServiceChargeRepository : BaseRepository<WarehouseDbContext, ServiceCharge>, IServiceChargeRepository
    {
        public ServiceChargeRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
