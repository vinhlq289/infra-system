﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class PackageDetailRepository : BaseRepository<WarehouseDbContext, PackageDetail>, IPackageDetailRepository
    {
        public PackageDetailRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
