﻿using InfraSystem.Wh.IRepository;
using Core.Repository.Abstract;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.Repository
{
    public class WarehouseRepository : BaseRepository<WarehouseDbContext, Warehouse>, IWarehouseRepository
    {
        public WarehouseRepository(WarehouseDbContext dbContext) : base(dbContext)
        {
        }
    }
}
