﻿using ProtoBuf;

namespace InfraSystem.Cache.Model
{
    [ProtoContract]
    public class WarehouseLocationCacheModel
    {
        [ProtoMember(1)]
        public string LocationName { get; set; }
        [ProtoMember(2)]
        public string LocationCode { get; set; }
        [ProtoMember(3)]
        public string CustomerCode { get; set; }
        [ProtoMember(4)]
        public int? TotalLocation { get; set; }
        [ProtoMember(5)]
        public decimal? TotalGrossWeight { get; set; }
        [ProtoMember(6)]
        public decimal? remainingWeight { get; set; }
        [ProtoMember(7)]
        public string postOfficeCode { get; set; }
    }
}
