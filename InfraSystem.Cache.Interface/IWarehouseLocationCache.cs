﻿using InfraSystem.Cache.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Interface
{
    public interface IWarehouseLocationCache : IBaseCache<IList<WarehouseLocationCacheModel>, int>
    {
        Task<bool> HashSet(string key, string model, string languageId = "");

        Task<bool> StringSet(string key, IList<WarehouseLocationCacheModel> models, string languageId = "");
        Task<bool> StringSetSinger(string key, WarehouseLocationCacheByLocationName model, string languageId = "");
        Task<WarehouseLocationCacheByLocationName> StringGetSinger(string key);
        Task<IList<WarehouseLocationPostOffice>> StringGetByPostOffice(string key);
        Task<WarehouseLocationCacheModel> StringGetSingerByCustomer(string key);
        Task<bool> StringSetSingerByCustomer(string key, WarehouseLocationCacheModel model, string languageId = "");
    }
}
