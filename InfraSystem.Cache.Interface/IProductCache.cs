﻿using InfraSystem.Cache.Model;
using System.Threading.Tasks;

namespace InfraSystem.Cache.Interface
{
    public interface IProductCache : IBaseCache<ProductCacheModel, string>
    {
        Task<ProductCacheModel> GetByProductCodeAsync(string code);
        Task<bool> SetProductCache(ProductCacheModel model);
        Task ClearProductCache();
    }
}
