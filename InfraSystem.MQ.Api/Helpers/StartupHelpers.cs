﻿using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using Core.Repository.Abstract;
using Core.Repository.Interface;
using GreenPipes;
using InfraSystem.MQ.Worker;
using InfraSystem.MQ.Worker.Consumer;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;
using InfraSystem.Om.IService;
using InfraSystem.Om.Repository;
using InfraSystem.Om.Service;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;
using InfraSystem.Wh.Repository;
using InfraSystem.Wh.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using OrderService = InfraSystem.Om.Service.OrderService;
using QueueSettings = InfraSystem.MQ.Worker.Configs.QueueSettings;

namespace InfraSystem.MQ.Api.Helpers
{
    public static class StartupHelpers
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            #region Warehouse

            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<IPackageDetailRepository, PackageDetailRepository>();
            services.AddTransient<IPackageDetailService, PackageDetailService>();
            services.AddTransient<IPackageRepository, PackageRepository>();
            services.AddTransient<IPackageService, PackageService>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IServiceChargePackageMappingRepository, ServiceChargePackageMappingRepository>();
            services.AddTransient<IServiceChargePackageMappingService, ServiceChargePackageMappingService>();
            services.AddTransient<IServiceChargeRepository, ServiceChargeRepository>();
            services.AddTransient<IServiceChargeService, ServiceChargeService>();
            services.AddTransient<IWarehouseRepository, WarehouseRepository>();
            services.AddTransient<IWarehouseService, WarehouseService>();

            #endregion

            #region OM

            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IOrderDetailRepository, OrderDetailRepository>();
            services.AddTransient<IOrderDetailService, OrderDetailService>();
            services.AddTransient<IOrderServiceRepository, OrderServiceRepository>();
            services.AddTransient<IOrderServiceService, OrderServiceService>();
            services.AddTransient<IOrderServiceMappingRepository, OrderServiceMappingRepository>();
            services.AddTransient<IOrderServiceMappingService, OrderServiceMappingService>();

            #endregion
            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "InfraSystem MQ Api",
                    Version = "v1"
                });
            });
            return services;
        }

        public static void UseSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InfraSystem Warehouse MQ Api");
                c.DocumentTitle = "InfraSystem MQ Api";
            });
        }

        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<WarehouseDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies().UseSqlServer(configuration.GetConnectionString("Warehouse"));
            });

            services.AddDbContext<IchibaCustomerContext>(opt =>
            {
                opt.UseLazyLoadingProxies()
                    .UseSqlServer(configuration.GetConnectionString("Warehouse"));
                opt.EnableSensitiveDataLogging();
            }, optionsLifetime: ServiceLifetime.Scoped);

            return services;
        }

        public static IServiceCollection AddMassTransit(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Worker.Configs.QueueSettings>(configuration.GetSection("AppSettings:QueueSettings"));
            var serviceProvider = services.BuildServiceProvider();
            var rabbitMqConfig = serviceProvider.GetRequiredService<IOptions<QueueSettings>>().Value;
            services.AddSingleton<IHostedService, ConsumerWorker>();

            services.AddScoped<AddPackageConsumer>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<AddPackageConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(rabbitMqConfig.HostName, rabbitMqConfig.VirtualHost, h =>
                    {
                        h.Username(rabbitMqConfig.UserName);
                        h.Password(rabbitMqConfig.Password);
                    });

                    cfg.ReceiveEndpoint(rabbitMqConfig.WarehousePackageNameQueue, e =>
                    {
                        e.PrefetchCount = 1;
                        e.UseRetry(r => r.Interval(3, TimeSpan.FromSeconds(3)));
                        e.UseRateLimit(1000, TimeSpan.FromSeconds(5));
                        e.ConfigureConsumer<AddPackageConsumer>(context);
                    });
                });
            });

            services.AddMassTransitHostedService();

            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork<WarehouseDbContext>>();

            return services;
        }
    }
}
