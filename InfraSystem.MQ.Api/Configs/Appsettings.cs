﻿namespace InfraSystem.MQ.Api.Configs
{
    public class AppSettings
    {
        public AppSettings()
        {
        }

        public AppSettings(QueueSettings queueSettings)
        {
            QueueSettings = queueSettings;
        }

        public QueueSettings QueueSettings { get; set; }
    }
}
