﻿using Core.Repository.Abstract;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;

namespace InfraSystem.Om.Repository
{
    public class OrderServiceRepository : BaseRepository<IchibaCustomerContext, OrderService>, IOrderServiceRepository
    {
        public OrderServiceRepository(IchibaCustomerContext dbContext) : base(dbContext)
        {
        }
    }
}
