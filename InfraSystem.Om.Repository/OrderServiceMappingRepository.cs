﻿using Core.Repository.Abstract;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;

namespace InfraSystem.Om.Repository
{
    public class OrderServiceMappingRepository : BaseRepository<IchibaCustomerContext, OrderServiceMapping>, IOrderServiceMappingRepository
    {
        public OrderServiceMappingRepository(IchibaCustomerContext dbContext) : base(dbContext)
        {
        }
    }
}
