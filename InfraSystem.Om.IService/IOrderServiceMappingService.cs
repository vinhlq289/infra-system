﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InfraSystem.Om.Domain;

namespace InfraSystem.Om.IService
{
    public interface IOrderServiceMappingService
    {
        Task<List<OrderServiceMapping>> GetByOrderId(int orderId);
    }
}
