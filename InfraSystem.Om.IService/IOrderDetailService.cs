﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InfraSystem.Om.Domain;

namespace InfraSystem.Om.IService
{
    public interface IOrderDetailService
    {
        Task<List<Orderdetail>> GetByOrderId(int orderId);
    }
}
