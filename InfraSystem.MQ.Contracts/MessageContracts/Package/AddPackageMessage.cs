﻿using System.Collections.Generic;
using InfraSystem.MQ.Contracts.Models;

namespace InfraSystem.MQ.Contracts
{
   public class AddPackageMessage
    {
        public PackageViewModel PackageViewModel { get; set; }
        public List<ProductViewModel> ProductViewModels { get; set; }
        public List<ServicePackageViewModel> ServicePackageViewModels { get; set; }
    }
}
