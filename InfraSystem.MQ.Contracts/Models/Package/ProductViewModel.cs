﻿namespace InfraSystem.MQ.Contracts.Models
{
    public class ProductViewModel
    {
        public string Code { get; set; }
        public int Type { get; set; }
        public int? Origin { get; set; }
        public string Name { get; set; }
        public string NameCustom { get; set; }
        public string Images { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceCustom { get; set; }
        public string ManufacturerPartNumber { get; set; }
        public string Url { get; set; }
        public string ProductUnit { get; set; }
        public int? QtyPerUnit { get; set; }
    }
}
