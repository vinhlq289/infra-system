﻿namespace InfraSystem.Driver.WhCode.Common
{
    public class BaseResponse<T> where T : class
    {
        public T data { get; set; }
        public bool status { get; set; }
        public string errorCode { get; set; }
    }

    public class BaseResponse : BaseResponse<object>
    {
    }
}
