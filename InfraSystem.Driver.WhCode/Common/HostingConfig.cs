﻿namespace InfraSystem.Driver.WhCode.Common
{
    /// <summary>
    /// Represents startup hosting configuration parameters
    /// </summary>
    public partial class HostingConfig
    {
        public const string Hosting = "Hosting";

        /// <summary>
        /// Gets or sets custom forwarded HTTP header (e.g. CF-Connecting-IP, X-FORWARDED-PROTO, etc)
        /// </summary>
        public string ForwardedHttpHeader { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use HTTP_CLUSTER_HTTPS
        /// </summary>
        public bool UseHttpClusterHttps { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use HTTP_X_FORWARDED_PROTO
        /// </summary>
        public bool UseHttpXForwardedProto { get; set; }


        public ApisConfig ApisConfig { get; set; }
    }

    public partial class ApisConfig
    {
        public string AutoNumber { get; set; }

        // Master 
        public string MasterApi { get; set; }
        public string MasterApiPrivate { get; set; }
        
        // OP 
        public string OpApi { get; set; }
        public string OpApiPrivate { get; set; }
        public string PartnerApi { get; set; }
        public string PartnerApiPrivate { get; set; }
        
        public string WhApi { get; set; }
        public string WhApiPrivate { get; set; }
        
        // MQ
        /// <summary>
        /// Api nhận thông tin => Mesage Queue
        /// </summary>
        public string MqApi { get; set; }
        /// <summary>
        /// Đọc thông tin từ redis do worker đã xử lý
        /// </summary>
        public string MqApiPrivate { get; set; }
        /// <summary>
        /// Đọc thông tin từ redis do worker đã xử lý
        /// </summary>
        public string MqApiPublic { get; set; }




    }

}
