﻿using System.Threading.Tasks;

using InfraSystem.Driver.WhCode.Models.Requests;
using InfraSystem.Driver.WhCode.Models.Response;

namespace InfraSystem.Driver.WhCode.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWhCodeService
    {
        /// <summary>
        ///  Gen Shipment Code
        /// </summary>
        /// <returns></returns>
        Task GenShipmentCodeAsync(GenShipmentCodeRequest model);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<ShipmentCodeUseStatusResponse> GetShipmentCodeStatusAsync(ShipmentCodeUseStatusRequest model);
    }
}
