﻿namespace InfraSystem.Driver.WhCode.Models.Requests
{
    public class ShipmentCodeUseStatusRequest
    {
        public string PostCode { get; set; }
        public int Year { get; set; }
        public string CountryCode { get; set; }
        public string Alphabet { get; set; }
    }
}
