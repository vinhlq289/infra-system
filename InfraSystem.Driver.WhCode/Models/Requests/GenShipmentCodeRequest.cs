﻿namespace InfraSystem.Driver.WhCode.Models.Requests
{
    public class GenShipmentCodeRequest
    {
        public string PostCode { get; set; }
        public string CountryCode { get; set; }
        public char Alphabet { get; set; }
        public int TotalNumber { get; set; }
    }
}
