﻿namespace InfraSystem.Driver.WhCode.Models.Response
{
    public class ShipmentCodeUseStatusResponse
    {
        public string Alphabet { get; set; }
        public string PostCode { get; set; }
        public int Year { get; set; }
        public string CountryCode { get; set; }
        public int Total { get; set; }
        public int Used { get; set; }
        public int Remain { get; set; }
    }
}
