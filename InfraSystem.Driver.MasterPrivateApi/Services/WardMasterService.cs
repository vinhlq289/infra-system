﻿using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using System.Web;
using Microsoft.AspNetCore.Http;
using InfraSystem.Domain.Master.Entity;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{

    public interface IWardMasterService
    {
        Task<Ward> FindByCodeAsync(string districtId, string code);
        Task<Ward> GetByIdAsync(string id);
    }
    public class WardMasterService : BaseService, IWardMasterService
    {
        public WardMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice, IWorkContext workContext)
            : base(httpClient, authorizeservice, workContext)
        {
        }

        public async Task<Ward> FindByCodeAsync(string districtId, string code)
        {
            string queryUrl = $"districtId={districtId ?? ""}&code={code ?? ""}";
            var response = await base.Get<DataResult<Ward>>(MasterPrivateApiEndPoints.Ward.FindByCode + "?" + queryUrl);
            return response?.data;
        }

        public async Task<Ward> GetByIdAsync(string id)
        {
            var value = HttpUtility.UrlEncode(id);
            var response = await base.Get<DataResult<Ward>>(MasterPrivateApiEndPoints.Ward.GetById + "?id=" + value);
            return response?.data;
        }
    }
}
