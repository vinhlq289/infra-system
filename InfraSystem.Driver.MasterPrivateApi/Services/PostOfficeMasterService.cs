﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using System.Web;
using Microsoft.AspNetCore.Http;
using InfraSystem.Domain.Master.Entity;
using System.Linq;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{
    public interface IPostOfficeMasterService
    {
        Task<IList<PostOffice>> GetAllAsync();
        Task<IList<PostOffice>> GetByCountryAsync(string countryId);
        Task<string> GetCountryCodeByPostCodeAsync(string postOfficeToSendCode);
        Task<PostOffice> GetByCodeAsync(string postOfficeToSendCode);
    }
    public class PostOfficeMasterService : BaseService, IPostOfficeMasterService
    {
        public PostOfficeMasterService(
            IMasterPrivHttpClient httpClient,
            IAuthorizeService authorizeservice,
            IWorkContext workContext 
        )
         : base(httpClient, authorizeservice, workContext)
        {
        }

        public async Task<IList<PostOffice>> GetAllAsync()
        {
            var response = await base.Get<DataResult<IEnumerable<PostOffice>>>(MasterPrivateApiEndPoints.PostOffice.GetAll);
            return response?.data?.ToList();
        }

        public async Task<PostOffice> GetByCodeAsync(string postOfficeToSendCode)
        {
            string urlQuery = $"code={HttpUtility.UrlEncode(postOfficeToSendCode)}";
            var response = await base.Get<DataResult<PostOffice>>(MasterPrivateApiEndPoints.PostOffice.GetByCode + "?" + urlQuery);
            return response?.data;
        }

        public async Task<IList<PostOffice>> GetByCountryAsync(string countryId)
        {
            var response = await base.Get<DataResult<IEnumerable<PostOffice>>>(MasterPrivateApiEndPoints.PostOffice.GetByCountry + "?countryId=" + countryId);
            return response?.data?.ToList();
        }

        public async Task<string> GetCountryCodeByPostCodeAsync(string postCode)
        {
            var response = await base.Get<DataResult<string>>(MasterPrivateApiEndPoints.PostOffice.GetCountryCodeByPostCode + "?postCode=" + postCode);
            return response?.data;
        }
    }
}
