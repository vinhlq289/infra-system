﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{
    public interface ISpPartnerMasterService
    {
        Task<IList<IChibaListItem>> GetSelectShipppingMethodAsync();

        Task<IList<IChibaListItem>> GetSelectPartnerTypesAsync(string cargoShippingMethod);

        Task<IList<IChibaListItem>> GetSelectPartnersAsync(string cargoShippingMethod, string[] types);
    }
    public class SpPartnerMasterService : BaseService, ISpPartnerMasterService
    {
        public SpPartnerMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice, IWorkContext workContext )
            : base(httpClient, authorizeservice,workContext  )
        {
        }

        public async Task<IList<IChibaListItem>> GetSelectShipppingMethodAsync()
        {
            var response = await base.Get<DataResult<IList<IChibaListItem>>>(MasterPrivateApiEndPoints.SpPartner.GetSelectShippingMethods);
            return response?.data;

        }
        public async Task<IList<IChibaListItem>> GetSelectPartnerTypesAsync(string cargoShippingMethod)
        {
            var strParams = string.IsNullOrWhiteSpace(cargoShippingMethod)
                ? ""
                : base.BuildParameter(new Dictionary<string, object>() {
                      {"cargoShippingMethod" , cargoShippingMethod}
                      });

            var response = await base.Get<DataResult<IList<IChibaListItem>>>(MasterPrivateApiEndPoints.SpPartner.GetSelectPartnerByTypes + "?" + strParams);
            return response?.data;
        }
        public async Task<IList<IChibaListItem>> GetSelectPartnersAsync(string cargoShippingMethod, string[] types)
        {
            var dicParam = new Dictionary<string, object>();
            dicParam.Add("cargoShippingMethod", cargoShippingMethod?.Trim() ?? "");
            string strParams = base.BuildParameter(dicParam);
            if (types != null && types.Length > 0)
            {
                foreach (var type in types)
                {
                    if (!string.IsNullOrWhiteSpace(type))
                        strParams += "&types=" + HttpUtility.UrlEncode(type.Trim());
                }
            }

            var response = await base.Get<DataResult<IList<IChibaListItem>>>(MasterPrivateApiEndPoints.SpPartner.GetSelectPartners + "?" + strParams);
            return response?.data;
        }
    }
}
