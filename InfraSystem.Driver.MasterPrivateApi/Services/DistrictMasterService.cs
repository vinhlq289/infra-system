﻿using System.Threading.Tasks;
using InfraSystem.Driver.MasterPrivateApi.Common;
using System.Web;
using Microsoft.AspNetCore.Http;
using InfraSystem.Domain.Master.Entity;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{

    public interface IDistrictMasterService
    {
        Task< District> FindByCodeAsync( string stateProvinceId , string code);
        Task<  District> GetByIdAsync(string id);
    }
    public class DistrictMasterService : BaseService, IDistrictMasterService
    {
        public DistrictMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice, IWorkContext workContext )
            : base(httpClient, authorizeservice,workContext )
        {
        }

        public async Task<District> FindByCodeAsync(string stateProvinceId, string code)
        {
            string strQuery  = $"stateProvince={HttpUtility.UrlEncode(stateProvinceId?.Trim() ?? "")}&code={HttpUtility.UrlEncode(code?.Trim()?? "")}";
            var response = await base.Get<DataResult< District>>(MasterPrivateApiEndPoints.District.FindByCode+"?"+strQuery);
            return response?.data ;
        }
         

        public async Task< District> GetByIdAsync(string id)
        {
            var value = HttpUtility.UrlEncode(id);
            var response = await base.Get<DataResult<District >>(MasterPrivateApiEndPoints.District.GetById+"?id="+value);
            return response?.data ;
        }
    }
}
