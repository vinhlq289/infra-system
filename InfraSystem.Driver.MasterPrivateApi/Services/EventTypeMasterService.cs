﻿using System.Collections.Generic;
using System.Threading.Tasks;

using InfraSystem.Domain.Master.Entity;
using InfraSystem.Driver.MasterPrivateApi.Common;

using Microsoft.AspNetCore.Http;

namespace InfraSystem.Driver.MasterPrivateApi.Services
{

    public interface IEventTypeMasterService
    {
        Task<IEnumerable<EventType>> GetAll();   
        Task<IEnumerable<EventType>> GetAllGroup();
    }

    public class EventTypeMasterService : BaseService, IEventTypeMasterService
    {
        public EventTypeMasterService(IMasterPrivHttpClient httpClient, IAuthorizeService authorizeservice, IWorkContext workContext)
               : base(httpClient, authorizeservice, workContext)
        {
        }

        public async Task<IEnumerable<EventType>> GetAll()
        {
            var response = await base.Get<DataResult<IEnumerable<EventType>>>(MasterPrivateApiEndPoints.EventType.GetAll);
            return response?.data;
        }

        public async Task<IEnumerable<EventType>> GetAllGroup()
        {
            var response = await base.Get<DataResult<IEnumerable<EventType>>>(MasterPrivateApiEndPoints.EventType.GetAllGroup);
            return response?.data;
        }

    }
}
