﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfraSystem.Driver.MasterPrivateApi.Common
{
    /// <summary>
    /// Loại Entity
    /// </summary>
    public enum EntityType
    {
        Bank = 10,
        BankAccount = 20,
        BankBranch = 30,
        Airline = 40,
        CargoAddService = 50,
        CargoSPService = 55,
        ChargesGroup = 60,
        ChargesType = 70,
        Commodity = 80,
        CommodityGroup = 85,
        Consignee = 90,
        Country = 100,
        CustomAgent = 110,
        DeliveryTime = 120,
        District = 130,
        EventType = 140,
        ForwardingAgent = 150,
        GlobalZone = 160,
        Incoterm = 170,
        MeasureDimension = 180,
        MeasureWeight = 190,
        PackageType = 200,
        PaymentMethod = 210,
        PaymentTerm = 220,
        Port = 230,
        QuantityUnit = 240,
        Shipper = 250,
        ShippingAgent = 260,
        ShippingLine = 270,
        SPCustomer = 280,
        SPMeasurement = 290,
        SPMoveType = 300,
        SPProductType = 310,
        SPSpecialServiceType = 330,
        StateProvince = 340,
        Trucker = 350,
        User = 360,
        VatPercentage = 370,
        VatType = 380,
        Vendor = 390,
        Vessel = 400,
        Ward = 410,
        SPAddress = 420,
        SPBilling = 430,
        Currency = 440,
        GenericAttribute = 450,
        Language = 460,
        LocaleStringResource = 470,
        LocalizedProperty = 480,
        PostOffice = 490,
        Warehouse = 500,
    }

    /// <summary>
    /// Loại địa chỉ
    /// </summary>
    public enum AddressType
    {
        /// <summary>
        /// Địa chỉ chính
        /// </summary>
        Main = 0,

        /// <summary>
        /// Địa chỉ hóa đơn
        /// </summary>
        Billing = 10,

        /// <summary>
        /// Địa chỉ pickup
        /// </summary>
        PickupDelivery = 20,

        /// <summary>
        /// Địa chỉ khác
        /// </summary>
        Others = 99
    }

    /// <summary>
    /// Hình thức vận chuyển hàng hóa XNK
    /// </summary>
    public enum CargoShippingMethod
    {
        /// <summary>
        /// Hàng không
        /// </summary>
        Air = 1,

        /// <summary>
        /// Hàng hải
        /// </summary>
        Ocean = 2,

        /// <summary>
        /// Nội địa
        /// </summary>
        Inland = 3
    }

    /// <summary>
    /// Loại hàng hóa XNK
    /// </summary>
    public enum CargoType
    {
        /// <summary>
        /// Hàng thông thường
        /// </summary>
        Document = 10,

        /// <summary>
        /// Hàng TMĐT
        /// </summary>
        ECommerce = 20,

        /// <summary>
        /// Hàng quà biếu tặng
        /// </summary>
        Gift = 30,

        /// <summary>
        /// Hàng mẫu
        /// </summary>
        Sample = 40,

        /// <summary>
        /// Hàng chuyển hoàn
        /// </summary>
        RefundedGoods = 50,

        /// <summary>
        /// Khác
        /// </summary>
        Other = 60
    }

    /// <summary>
    /// Trạng thái Shipment
    /// </summary>
    public enum ShipmentStatus
    {
        /// <summary>
        /// Khởi tạo thông tin Shipment
        /// </summary>
        ShipmentCreated = 0,

        /// <summary>
        /// Chấp nhận tại TTKT gửi<br/>
        /// Accepted at sending operation center
        /// </summary>
        AcceptedAtSendingOPCenter = 5,

        /// <summary>
        /// Đã dừng khai thác: được dùng khi shipment này đã được xử lý tách/gộp thành shipment khác
        /// </summary>
        HasStoppedMining = 7,

        /// <summary>
        /// WattingInfomation - Chờ bổ xung thông tin
        ///  Khi Khởi tạo shipment từ App mà không bổ xung thông tin
        /// </summary>
        WattingInfomation = 8,
        /// <summary>
        ///  AddedInfomation  -  Đã cập nhật thông tin từ đối tác
        ///  Khi nhận được thông tin từ partner
        /// </summary>
        PartnerAddedInfomation = 9,

        /// <summary>
        /// Shipment chờ xử lý
        /// </summary>
        WaitProcess = 10,

        /// <summary>
        /// Shipment đã được xử lý
        /// </summary>
        ShipmentProcessed = 20,

        /// <summary>
        /// Đã đóng túi<br/>
        /// Bagged
        /// </summary>
        Bagged = 30,

        /// <summary>
        /// Đã đóng chuyến thư nội địa<br/>
        /// Shipment closed
        /// </summary>
        DispatchClosedInland = 40,

        /// <summary>
        /// Đã đến TTKT đích - nội địa<br/>
        /// Arrived at destination operation center inland
        /// </summary>
        ArrivedAtDestinationOperationCenterInland = 50,

        /// <summary>
        /// Đã đóng chuyến thư quốc tế
        /// </summary>
        DispatchClosedInternational = 60,

        /// <summary>
        /// Đang làm thủ tục hải quan xuất khẩu<br/>
        /// Doing export customs procedures
        /// </summary>
        DoingExportCustomsProcedures = 70,

        /// <summary>
        /// Thông quan xuất khẩu thành công<br/>
        /// Export customs clearance succeeded
        /// </summary>
        ExportCustomsClearanceSucceeded = 80,

        /// <summary>
        /// Cần bổ sung giấy tờ thông quan<br/>
        /// Need additional clearance documents
        /// </summary>
        NeedAddCustomsClearanceDocs = 90,

        /// <summary>
        /// Thông quan xuất khẩu không thành công<br/>
        /// Export customs clearance rejected
        /// </summary>
        ExportCustomsClearanceRejected = 100,

        /// <summary>
        /// Đang chờ thủ tục xuất khẩu
        /// </summary>
        WaitingForExportProcedures = 110,

        /// <summary>
        /// Đã sắp xếp chuyến bay<br/>
        /// Flight arranged
        /// </summary>
        FlightArranged = 120,

        /// <summary>
        /// Đã sắp xếp chuyến tàu biển<br/>
        /// Vessel arranged
        /// </summary>
        VesselArranged = 130,

        /// <summary>
        /// Đã sắp xếp chuyến xe<br/>
        /// Trucker arranged
        /// </summary>
        TruckerArranged = 140,

        /// <summary>
        /// Đã giao Hàng không<br/>
        /// Delivered to airline
        /// </summary>
        DeliveredToAirline = 150,

        /// <summary>
        /// Đã giao Hãng tàu biển<br/>
        /// Delivered to shipping company
        /// </summary>
        DeliveredToShippingCompany = 160,

        /// <summary>
        /// Đã giao Hãng xe<br/>
        /// Delivered to trucking company
        /// </summary>
        DeliveredToTruckingCompany = 170,

        /// <summary>
        /// Đã đến nước đích<br/>
        /// Arrived at destination country
        /// </summary>
        ArrivedAtDestinationCountry = 180,

        /// <summary>
        /// Đang làm thủ tục hải quan nhập khẩu<br/>
        /// Doing import customs procedures
        /// </summary>
        DoingImportCustomsProcedures = 190,

        /// <summary>
        /// Thông quan nhập khẩu thành công<br/>
        /// Import customs clearance succeeded
        /// </summary>
        ImportCustomsClearanceSucceeded = 200,

        /// <summary>
        /// Thông quan nhập khẩu không thành công<br/>
        /// Import customs clearance rejected
        /// </summary>
        ImportCustomsClearanceRejected = 210,

        /// <summary>
        /// Đang chờ thủ tục nhập khẩu
        /// </summary>
        WaitingForImportProcedures = 220,


        ///// <summary>
        ///// Bị từ chối thông quan<br/>
        ///// Customs clearance rejected
        ///// </summary>
        //CustomsClearanceRejected = 120,
        /// <summary>
        /// Đã đến TTKT đích - quốc tế<br/>
        /// Arrived at destination operation center international
        /// </summary>
        ArrivedAtDestinationOperationCenterInternational = 230,

        /// <summary>
        /// Đã giao cho đối tác phát hàng
        /// </summary>
        DeliveredToDeliveryPartner = 240,

        ///// <summary>
        ///// Đang sắp xếp giao hàng<br/>
        ///// Arranging delivery
        ///// </summary>
        //ArrangingDelivery = 140,
        /// <summary>
        /// Đã giao hàng<br/>
        /// Delivered
        /// </summary>
        Delivered = 250,

        /// <summary>
        /// Giao hàng không thành công<br/>
        /// Delivery failed
        /// </summary>
        DeliveryFailed = 260,

        /// <summary>
        /// Đã chuyển hoàn<br/>
        /// Refunded
        /// </summary>
        Refunded = 270,

        /// <summary>
        /// Đã hủy<br/>
        /// Cancelled
        /// </summary>
        Cancelled = 280,
    }

 

}
