﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace InfraSystem.Driver.MasterPrivateApi.Common
{

    public class MasterPrivateWebWorkContextDelegatingHandler : DelegatingHandler
    {
        private IWorkContext _workContext;

        public MasterPrivateWebWorkContextDelegatingHandler(IWorkContext workContext)
        {
            _workContext = workContext;
        }


        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // if(_workContext == null ) _workContext = EngineContext.Current.Resolve<IWorkContext>();

            string langId = _workContext?.LanguageId;
            string postOfficeId = "";
            string userId = _workContext?.UserId;
            string userName = _workContext?.UserName;

            request.Headers.Add(IChibaHeaderNames.LanguageId, langId);
            request.Headers.Add(IChibaHeaderNames.PostOfficeId, postOfficeId);
            request.Headers.Add(IChibaHeaderNames.UserId, userId);
            request.Headers.Add(IChibaHeaderNames.UserName, userName);

            return await base.SendAsync(request, cancellationToken);
            // .ConfigureAwait(false);
        }
    }


    public interface IWorkContext
    {
        string LanguageId { get; }

        string UserId { get; }

        string UserName { get; }

        string PostOfficeId { get; }

    }
}
