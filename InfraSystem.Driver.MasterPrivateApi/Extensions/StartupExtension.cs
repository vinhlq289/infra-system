﻿using System;

using InfraSystem.Driver.MasterPrivateApi.Common;
using InfraSystem.Driver.MasterPrivateApi.IServices;
using InfraSystem.Driver.MasterPrivateApi.Services;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Polly;

using RestEase.HttpClientFactory;

namespace InfraSystem.Driver.MasterPrivateApi.Extensions
{
    public static class StartupExtension
    { 
        public static IServiceCollection AddMasterPrivateServiceApi(this IServiceCollection services, IConfiguration configuration)
        {
            //Config
            var hostingConfig = configuration.GetSection(HostingConfig.Hosting).Get<HostingConfig>();
            var masterPrivateApiConfigSection = configuration.GetSection(MasterPrivateApiEndPoints.SettingKey);


            // HttpClient
            services.AddSingleton<IMasterPrivResilientHttpClientFactory, MasterPrivResilientHttpClientFactory>(sp =>
            {
                const int retryCount = 3;
                const int exceptionsAllowedBeforeBreaking = 5;
                string baseUrl = hostingConfig.ApisConfig.MasterApiPrivate;
                var logger  = sp.GetRequiredService<ILogger<MasterPrivResilientHttpClient>>();
                return new MasterPrivResilientHttpClientFactory(logger, exceptionsAllowedBeforeBreaking, retryCount, baseUrl: baseUrl);
            });
            services.AddSingleton<IMasterPrivHttpClient, MasterPrivResilientHttpClient>
                (sp => sp.GetService<IMasterPrivResilientHttpClientFactory>().CreateResilientHttpClient());

            // Master Service 
            services.AddSingleton<IAuthorizeService, AuthorizeService>();
            services.AddSingleton<ICargoAddMasterService, CargoAddMasterService>();
            services.AddSingleton<ICargoSpMasterService, CargoSpMasterService>();
            services.AddSingleton<ICommodityMasterService, CommodityMasterService>();
            services.AddSingleton<ICountryMasterService, CountryMasterService>();
            services.AddSingleton<IDistrictMasterService, DistrictMasterService>();
            services.AddSingleton<IEventTypeMasterService, EventTypeMasterService>();
            services.AddSingleton<IIncotermMasterService, IncotermMasterService>();
            services.AddSingleton<IPostOfficeMasterService, PostOfficeMasterService>();
            services.AddSingleton<ISpPartnerMasterService, SpPartnerMasterService>();
            services.AddSingleton<IStateProvinceMasterService, StateProvinceMasterService>();
            services.AddSingleton<IWardMasterService, WardMasterService>();

            // Using RestEasy
            services.AddMasterService<ICommodityGroupMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IConsigneeMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<ICurrencyMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<ICustomerMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IFloatingExchangeRateMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IMeasureDimensionMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IMeasureWeightMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IQuantityUnitMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IShipperMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<ISpAddressMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IWareHouseLocationMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IWarehouseMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddMasterService<IProductMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
             


            return services;

        }
         
        private static IServiceCollection AddMasterService<T>(this IServiceCollection services, string baseUrl) where T : class
        { 
            // services.AddRestEaseClient<IMeasureWeightMasterService>(hostingConfig.ApisConfig.MasterApiPrivate);
            services.AddRestEaseClient<T>(baseUrl)
                .AddHttpMessageHandler<MasterPrivateWebWorkContextDelegatingHandler>()
                .AddTransientHttpErrorPolicy(builder =>
                    builder.WaitAndRetryAsync(new[]
                    {
                        TimeSpan.FromMilliseconds(100),
                        TimeSpan.FromMilliseconds(500),
                        TimeSpan.FromSeconds(2)
                    }));
            return services;
        }
    }
}
