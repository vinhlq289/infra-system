﻿namespace InfraSystem.Shared.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Empty to null
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string? NullEmpty(this string input)
        {
            return string.IsNullOrWhiteSpace(input) ? null : input.Trim();
        }
        
        /// <summary>
        /// Null to Emty
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string? EmptyNull(this string input)
        {
            return string.IsNullOrWhiteSpace(input) ? string.Empty : input.Trim();
        }
    }
}
