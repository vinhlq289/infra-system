﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InfraSystem.Shared.Dto
{
    public class QueryResult<T>
    {
        public QueryResult()
        {
        }

        public QueryResult(long count, IEnumerable<T> items)
        {
            Count = count;
            Items = items;
        }

        public long Count { get; set; }
        public IEnumerable<T> Items { get; set; } = Enumerable.Empty<T>();

        public static QueryResult<T> Empty()
        {
            return new QueryResult<T>(0, null);
        }
    }


    public static class QueryResultExtension
    {
        public static QueryResult<K> Select<T, K>(this QueryResult<T> @this, Func<T, K> selector)
        {
            return new QueryResult<K>
            {
                Count = @this.Count,
                Items = @this.Items.Select(selector)
            };
        }
    }
}
