﻿namespace InfraSystem.Shared.Constants
{
    public class ConfigurationKeys
    {
        public const string OPConnectionString = "OP";
        public const string MasterConnectionString = "Master";
    }
}
