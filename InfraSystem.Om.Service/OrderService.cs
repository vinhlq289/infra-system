﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;
using InfraSystem.Om.IService;

namespace InfraSystem.Om.Service
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<Order> GetByShipmentCode(string shipmentCode)
        {
            return _orderRepository.Find(new Specification<Order>(x => x.ReferenceCode.Equals(shipmentCode)))
                .FirstOrDefault();
        }

        public async Task<List<Order>> GetByTrackingCode(string trackingCode)
        {
            return _orderRepository.Find(new Specification<Order>(x => x.ReferenceCode.Equals(trackingCode))).ToList();
        }
    }
}
