﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Om.Domain;
using InfraSystem.Om.IRepository;
using InfraSystem.Om.IService;

namespace InfraSystem.Om.Service
{
    public class OrderDetailService: IOrderDetailService
    {
        private readonly IOrderDetailRepository _orderDetailRepository;

        public OrderDetailService(IOrderDetailRepository orderDetailRepository)
        {
            _orderDetailRepository = orderDetailRepository;
        }

        public async Task<List<Orderdetail>> GetByOrderId(int orderId)
        {
            return _orderDetailRepository.Find(new Specification<Orderdetail>(x => x.OrderId.Equals(orderId))).ToList();
        }
    }
}
