﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Category> GetByName(string categoryName)
        {
            return _categoryRepository.Find(new Specification<Category>(x => x.Name.Equals(categoryName))).FirstOrDefault();
        }

        public async Task<List<Category>> GetByNames(List<string> categoryNames)
        {
            return _categoryRepository.Find(new Specification<Category>(x => categoryNames.Contains(x.Name))).ToList();
        }
    }
}
