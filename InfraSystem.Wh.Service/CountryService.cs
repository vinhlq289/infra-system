﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        public CountryService(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public async Task<List<Country>> GetByCode(List<string> codes)
        {
            return _countryRepository.Find(new Specification<Country>(x => codes.Contains(x.Code))).ToList();
        }
    }
}
