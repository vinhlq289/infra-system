﻿using System.Threading.Tasks;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<Product> AddProduct(Product product)
        {
            return _productRepository.Add(product);
        }
    }
}
