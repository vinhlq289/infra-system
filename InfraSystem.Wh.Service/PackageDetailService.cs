﻿using System.Linq;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class PackageDetailService: IPackageDetailService
    {
        private readonly IPackageDetailRepository _packageDetailRepository;

        public PackageDetailService(IPackageDetailRepository packageDetailRepository)
        {
            _packageDetailRepository = packageDetailRepository;
        }

        public async Task<PackageDetail> AddPackageDetail(PackageDetail packageDetail)
        {
            return _packageDetailRepository.Add(packageDetail);
        }

        public async Task<PackageDetail> GetByOrderCode(string orderCode)
        {
            return _packageDetailRepository.Find(new Specification<PackageDetail>(x => x.OrderCode.Equals(orderCode)))
                .FirstOrDefault();
        }
    }
}
