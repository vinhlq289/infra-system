﻿using System.Threading.Tasks;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class ServiceChargePackageMappingService : IServiceChargePackageMappingService
    {
        private readonly IServiceChargePackageMappingRepository _chargePackageMappingRepository;

        public ServiceChargePackageMappingService(IServiceChargePackageMappingRepository chargePackageMappingRepository)
        {
            _chargePackageMappingRepository = chargePackageMappingRepository;
        }

        public async Task<ServiceChargePackageMapping> AddServiceChargePackageMapping(ServiceChargePackageMapping serviceChargePackageMapping)
        {
            return _chargePackageMappingRepository.Add(serviceChargePackageMapping);
        }
    }
}
