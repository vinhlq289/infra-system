﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Core.Common;
using Core.Specification.Abstract;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class PackageService: IPackageService
    {
        private readonly IPackageRepository _packageRepository;

        public PackageService(IPackageRepository packageRepository)
        {
            _packageRepository = packageRepository;
        }

        public async Task<Package> AddPackage(Package package)
        {
            return _packageRepository.Add(package);
        }

        public async Task<Package> GetByTrackingCode(string trackingCode)
        {
            return _packageRepository.Find(new Specification<Package>(x => x.TrackingCode.Equals(trackingCode)))
                .FirstOrDefault();
        }

        public async Task<Package> GetLeast(string warehouseCode)
        {
            return _packageRepository.Find(new Specification<Package>(x => true),
                new Sorts
                {
                    new Sort {SortBy = "CreatedDate", SortDirection = Sort.SORT_DIRECTION_DESC}
                },
                new Paging
                {
                    PageIndex = 0,
                    PageSize = 1
                }).FirstOrDefault();
        }
    }
}
