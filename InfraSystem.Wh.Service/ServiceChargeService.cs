﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Specification.Abstract;
using InfraSystem.Wh.Domain;
using InfraSystem.Wh.IRepository;
using InfraSystem.Wh.IService;

namespace InfraSystem.Wh.Service
{
    public class ServiceChargeService : IServiceChargeService
    {
        private readonly IServiceChargeRepository _serviceChargeRepository;

        public ServiceChargeService(IServiceChargeRepository serviceChargeRepository)
        {
            _serviceChargeRepository = serviceChargeRepository;
        }

        public async Task<ServiceCharge> AddServiceCharge(ServiceCharge serviceCharge)
        {
            return _serviceChargeRepository.Add(serviceCharge);
        }

        public async Task<List<ServiceCharge>> GetByCode(List<string> serviceChargeCodes)
        {
            return _serviceChargeRepository
                .Find(new Specification<ServiceCharge>(x => serviceChargeCodes.Contains(x.Code))).ToList();
        }

        public async Task<List<ServiceCharge>> GetById(List<int> serviceChargeIds)
        {
            return _serviceChargeRepository
                .Find(new Specification<ServiceCharge>(x => serviceChargeIds.Contains(x.Id))).ToList();
        }
    }
}
