﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IService
{
    public interface ICountryService
    {
        Task<List<Country>> GetByCode(List<string> codes);
    }
}
