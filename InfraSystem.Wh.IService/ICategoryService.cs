﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IService
{
    public interface ICategoryService
    {
        Task<Category> GetByName(string categoryName);
        Task<List<Category>> GetByNames(List<string> categoryNames);
    }
}
