﻿using InfraSystem.Wh.Domain;
using System.Threading.Tasks;

namespace InfraSystem.Wh.IService
{
    public interface IPackageDetailService
    {
        Task<PackageDetail> AddPackageDetail(PackageDetail packageDetail);
        Task<PackageDetail> GetByOrderCode(string orderCode);
    }
}
