﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IService
{
    public interface IServiceChargeService
    {
        Task<ServiceCharge> AddServiceCharge(ServiceCharge serviceCharge);
        Task<List<ServiceCharge>> GetByCode(List<string> serviceChargeCodes);
        Task<List<ServiceCharge>> GetById(List<int> serviceChargeIds);
    }
}
