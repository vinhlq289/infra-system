﻿using System.Threading.Tasks;
using InfraSystem.Wh.Domain;

namespace InfraSystem.Wh.IService
{
    public interface IWarehouseService
    {
        Task<Warehouse> GetByCode(string warehouseCode);
    }
}
