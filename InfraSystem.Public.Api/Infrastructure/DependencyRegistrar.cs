﻿//using Autofac;
//using IChiba.Core.Configuration;
//using IChiba.Core.Infrastructure;
//using IChiba.Core.Infrastructure.DependencyManagement;
//using IChiba.Data;
//using IChiba.Web.Framework.Infrastructure.Extensions;

//namespace InfraSystem.Private.Api.Infrastructure
//{
//    public class DependencyRegistrar : IDependencyRegistrar
//    {
//        /// <summary>
//        /// Register services and interfaces
//        /// </summary>
//        /// <param name="builder">Container builder</param>
//        /// <param name="typeFinder">Type finder</param>
//        /// <param name="config">Config</param>
//        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, IChibaConfig config)
//        {
//            // Data Layer
//            builder.RegisterDataConnection(new[]
//            {
//                DataConnectionHelper.ConnectionStringNames.Master,
//                DataConnectionHelper.ConnectionStringNames.OP
//            });

//            // Repositories
//            builder.RegisterRepository(new[]
//            {
//                DataConnectionHelper.ConnectionStringNames.Master,
//                DataConnectionHelper.ConnectionStringNames.OP
//            });

//            // Cache
//            builder.RegisterCacheManager();


//        }

//        /// <summary>
//        /// Gets order of this dependency registrar implementation
//        /// </summary>
//        public int Order => 2;
//    }
//}
