﻿using AutoMapper;

namespace InfraSystem.Private.Api.AutoMappers
{
    public class MappingToWarehouseLocationCacheByLocationName : Profile
    {
        public MappingToWarehouseLocationCacheByLocationName()
        {
        }

        private IMappingExpression<TFromModel, TToModel> CreateMapIgnore<TFromModel, TToModel>()
        {
            var map = CreateMap<TFromModel, TToModel>();

            map.ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            return map;
        }
    }
}
